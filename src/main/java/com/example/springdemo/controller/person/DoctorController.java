package com.example.springdemo.controller.person;


import com.example.springdemo.dto.medication.*;
import com.example.springdemo.dto.person.*;
import com.example.springdemo.services.medication.DrugService;
import com.example.springdemo.services.medication.MedicationPlanService;
import com.example.springdemo.services.medication.PrescriptionService;
import com.example.springdemo.services.person.CaregiverService;
import com.example.springdemo.services.person.DoctorService;
import com.example.springdemo.services.person.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctors")
public class DoctorController {

    private final DoctorService doctorService;
    private final CaregiverService caregiverService;
    private final PatientService patientService;
    private final DrugService drugService;
    private final PrescriptionService prescriptionService;
    private final MedicationPlanService medicationPlanService;

    @Autowired
    public DoctorController(DoctorService doctorService, CaregiverService caregiverService, PatientService patientService, DrugService drugService, PrescriptionService prescriptionService, MedicationPlanService medicationPlanService) {
        this.doctorService = doctorService;
        this.caregiverService = caregiverService;
        this.patientService = patientService;
        this.drugService = drugService;
        this.prescriptionService = prescriptionService;
        this.medicationPlanService = medicationPlanService;
    }

    /*
    Find doctor by id and get all
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public DoctorViewDTO findById(@PathVariable("id") Integer id){
        return doctorService.findUserById(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<DoctorViewDTO> findAllDoctors(){
        return doctorService.findAll();
    }

    @RequestMapping(value = "/username/{username}", method = RequestMethod.GET)
    public DoctorDTO getDoctorByUsername(@PathVariable("username") String username){
        return doctorService.getDoctorByUsername(username);
    }
//    /*
//    CRUD OPERATIONS on caregivers
//     */
//
//    @RequestMapping(value = "/all_caregivers", method = RequestMethod.GET)
//    public List<CaregiverViewDTO> findAllCaregivers(){
//        return caregiverService.findAll();
//    }
//
//    @RequestMapping(value = "/caregiver/{id}", method = RequestMethod.GET)
//    public CaregiverViewDTO findCaregiverById(@PathVariable("id") Integer id){
//        return caregiverService.findUserById(id);
//    }
//
//    @RequestMapping(value = "/insert_caregiver", method = RequestMethod.POST)
//    public Integer insertCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO){
//        return caregiverService.insert(caregiverDTO);
//    }
//
//    @RequestMapping(value = "/update_caregiver", method = RequestMethod.PUT)
//    public void updateCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO){
//        caregiverService.update(caregiverDTO);
//    }
//
//    @RequestMapping(value = "/delete_caregiver", method = RequestMethod.DELETE)
//    public void deleteCaregiverDTO(@RequestBody CaregiverViewDTO caregiverViewDTO){
//        caregiverService.delete(caregiverViewDTO);
//    }
//
//    /*
//    CRUD OPERATIONS ON PATIENTS
//     */
//
//    @RequestMapping(value = "/all_patients", method = RequestMethod.GET)
//    public List<PatientViewDTO> findAllPatients(){
//        return patientService.findAll();
//    }
//
//    @RequestMapping(value = "/patient/{id}", method = RequestMethod.GET)
//    public PatientViewDTO findPatientById(@PathVariable("id") Integer id){
//        return patientService.findUserById(id);
//    }
//
//    @RequestMapping(value = "/insert_patient", method = RequestMethod.POST)
//    public Integer insertPatientDTO(@RequestBody PatientDTO patientDTO){
//        return patientService.insert(patientDTO);
//    }
//
//    @RequestMapping(value = "/update_patient", method = RequestMethod.PUT)
//    public void updatePatientDTO(@RequestBody PatientDTO patientDTO){
//        patientService.update(patientDTO);
//    }
//
//    @RequestMapping(value = "/delete_patient", method = RequestMethod.DELETE)
//    public void deletePatientDTO(@RequestBody PatientViewDTO patientViewDTO){
//        patientService.delete(patientViewDTO);
//    }
//
//    /*
//    CRUD OPERATIONS ON DRUGS
//     */
//
//    @RequestMapping(value = "/all_drugs", method = RequestMethod.GET)
//    public List<DrugViewDTO> findAllDrugs(){
//        return drugService.findAll();
//    }
//
//    @RequestMapping(value = "/drug/{id}", method = RequestMethod.GET)
//    public DrugViewDTO findDrugById(@PathVariable("id") Integer id){
//        return drugService.findUserById(id);
//    }
//
//    @RequestMapping(value = "/insert_drug", method = RequestMethod.POST)
//    public Integer insertDrugDTO(@RequestBody DrugDTO drugDTO){
//        return drugService.insert(drugDTO);
//    }
//
//    @RequestMapping(value = "/update_drug", method = RequestMethod.PUT)
//    public void updateDrugDTO(@RequestBody DrugDTO drugDTO){
//        drugService.update(drugDTO);
//    }
//
//    @RequestMapping(value = "/delete_drug", method = RequestMethod.DELETE)
//    public void deleteDrugDTO(@RequestBody DrugViewDTO drugViewDTO){
//        drugService.delete(drugViewDTO);
//    }

     /*
    CRUD OPERATIONS ON Prescriptions
     */

    @RequestMapping(value = "/all_prescriptions", method = RequestMethod.GET)
    public List<PrescriptionViewDTO> findAllPrescriptions(){
        return prescriptionService.findAll();
    }

    @RequestMapping(value = "/prescription/{id}", method = RequestMethod.GET)
    public PrescriptionViewDTO findPrescriptionById(@PathVariable("id") Integer id){
        return prescriptionService.findUserById(id);
    }

    @RequestMapping(value = "/insert_prescription", method = RequestMethod.POST)
    public Integer insertPrescriptionDTO(@RequestBody PrescriptionDTO prescriptionDTO){
        return prescriptionService.insert(prescriptionDTO);
    }

    @RequestMapping(value = "/update_prescription", method = RequestMethod.PUT)
    public void updatePrescriptionDTO(@RequestBody PrescriptionDTO prescriptionDTO){
        prescriptionService.update(prescriptionDTO);
    }

    @RequestMapping(value = "/delete_prescription", method = RequestMethod.DELETE)
    public void deletePrescriptionDTO(@RequestBody PrescriptionViewDTO prescriptionViewDTO){
        prescriptionService.delete(prescriptionViewDTO);
    }

     /*
    CRUD OPERATIONS ON Medication Plan
     */

    @RequestMapping(value = "/all_medicationplan", method = RequestMethod.GET)
    public List<MedicationPlanViewDTO> findAllMedicationPlans(){
        return medicationPlanService.findAll();
    }

    @RequestMapping(value = "/medicationplan/{id}", method = RequestMethod.GET)
    public MedicationPlanViewDTO findMedicationPlanById(@PathVariable("id") Integer id){
        return medicationPlanService.findUserById(id);
    }

    @RequestMapping(value = "/insert_medicationplan", method = RequestMethod.POST)
    public Integer insertMedicationPlanDTO(@RequestBody MedicationPlanDTO medicationPlanDTO){
        return medicationPlanService.insert(medicationPlanDTO);
    }

    @RequestMapping(value = "/update_medicationplan", method = RequestMethod.PUT)
    public void updateMedicationPlanDTO(@RequestBody MedicationPlanDTO medicationPlanDTO){
        medicationPlanService.update(medicationPlanDTO);
    }

    @RequestMapping(value = "/delete_medicationplan", method = RequestMethod.DELETE)
    public void deleteMedicationPlanDTO(@RequestBody MedicationPlanViewDTO medicationPlanViewDTO){
        medicationPlanService.delete(medicationPlanViewDTO);
    }
}
