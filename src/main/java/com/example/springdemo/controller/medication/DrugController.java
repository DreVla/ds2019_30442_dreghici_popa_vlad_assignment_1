package com.example.springdemo.controller.medication;

import com.example.springdemo.dto.medication.DrugDTO;
import com.example.springdemo.dto.medication.DrugViewDTO;
import com.example.springdemo.dto.person.CaregiverViewDTO;
import com.example.springdemo.services.medication.DrugService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/drugs")
public class DrugController {

    private final DrugService drugService;

    public DrugController(DrugService drugService) {
        this.drugService = drugService;
    }

    /*
    Find drugs by id and get all
     */
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<DrugViewDTO> findAllDrugs(){
        return drugService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public DrugViewDTO findDrugById(@PathVariable("id") Integer id){
        return drugService.findUserById(id);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Integer insertDrugDTO(@RequestBody DrugDTO drugDTO){
        return drugService.insert(drugDTO);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public void updateDrugDTO(@RequestBody DrugDTO drugDTO){
        drugService.update(drugDTO);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void deleteDrugDTO(@RequestBody DrugViewDTO drugViewDTO){
        drugService.delete(drugViewDTO);
    }


}
