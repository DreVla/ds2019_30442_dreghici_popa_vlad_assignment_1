package com.example.springdemo.entities.person;


import com.example.springdemo.entities.enums.Gender;
import com.example.springdemo.entities.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.Set;

@Entity
@Table(name = "CAREGIVER")
public class Caregiver extends Person {

    @JsonIgnore
    @OneToMany(mappedBy="careGiver")
    private Set<Patient> patients;

    public Caregiver() {
    }

    public Caregiver(Integer id, String name, Set<Patient> patients) {
        super(id, name, Role.CAREGIVER);
        this.patients = patients;
    }

    public Caregiver(Integer id, String name, String birthdate, Gender gender, String username, String password, Set<Patient> patients) {
        super(id, name, birthdate, gender, Role.CAREGIVER, username, password);
        this.patients = patients;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }

    public void addPatient(Patient patient){
        this.patients.add(patient);
    }
}
