package com.example.springdemo.entities.person;

import com.example.springdemo.entities.enums.Gender;
import com.example.springdemo.entities.enums.Role;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@MappedSuperclass
public class Person {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 200)
    private String name;

    @Column(name = "birthdate", length = 100)
    private String birthdate;

    @Column(name = "gender")
    private Gender gender;

    @Column(name = "role")
    private Role role;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    public Person() {
    }

    public Person(Integer id, String name, Role role) {
        this.id = id;
        this.name = name;
        this.role = role;
    }

    public Person(Integer id, String name, String birthdate, Gender gender, Role role, String username, String password) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = Gender.MALE;
        this.role = role;
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", gender=" + gender +
                ", role=" + role +
                '}';
    }
}
