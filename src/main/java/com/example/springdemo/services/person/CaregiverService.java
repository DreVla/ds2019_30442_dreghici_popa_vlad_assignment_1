package com.example.springdemo.services.person;

import com.example.springdemo.dto.builders.person.CaregiverBuilder;
import com.example.springdemo.dto.builders.person.CaregiverViewBuilder;
import com.example.springdemo.dto.person.*;
import com.example.springdemo.entities.person.Caregiver;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.person.CaregiverRepository;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public CaregiverViewDTO findUserById(Integer id) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }
        return CaregiverViewBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<CaregiverViewDTO> findAll() {
        List<Caregiver> caregivers = caregiverRepository.getAllOrdered();

        return caregivers.stream()
                .map(CaregiverViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(CaregiverDTO caregiverDTO) {

        PersonFieldValidator.validateInsertOrUpdateCaregiver(caregiverDTO);

        return caregiverRepository
                .save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO))
                .getId();
    }

    public Integer update(CaregiverDTO caregiverDTO) {

        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getId());

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", caregiverDTO.getId().toString());
        }
        PersonFieldValidator.validateInsertOrUpdateCaregiver(caregiverDTO);

        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
    }

    public void delete(CaregiverViewDTO caregiverViewDTO) {
        this.caregiverRepository.deleteById(caregiverViewDTO.getId());
    }

    public CaregiverDTO getCaregiverByUsername(String username){
        Caregiver caregiver = caregiverRepository.getCaregiverByUsername(username);
        if(caregiver != null)
            return CaregiverViewBuilder.generateDTOwithPass(caregiver);
        else
            return null;
    }
}
