package com.example.springdemo.dto.medication;

import com.example.springdemo.entities.medication.Drug;
import com.example.springdemo.entities.medication.MedicationPlan;

import java.util.Objects;

public class PrescriptionDTO {

    private Integer id;
    private Drug drug;
    private String intake;
    private MedicationPlan medicationPlan;

    public PrescriptionDTO() {
    }

    public PrescriptionDTO(Integer id, Drug drug, String intake, MedicationPlan medicationPlan) {
        this.id = id;
        this.drug = drug;
        this.intake = intake;
        this.medicationPlan = medicationPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public String getIntake() {
        return intake;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrescriptionDTO that = (PrescriptionDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(drug, that.drug) &&
                Objects.equals(intake, that.intake) &&
                Objects.equals(medicationPlan, that.medicationPlan);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, drug, intake, medicationPlan);
    }
}
