package com.example.springdemo.dto.builders.medication;

import com.example.springdemo.dto.medication.DrugDTO;
import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.entities.medication.Drug;
import com.example.springdemo.entities.medication.MedicationPlan;

public class MedicationPlanBuilder {


    public MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medicationPlan){
        return new MedicationPlanDTO(
                medicationPlan.getId(),
                medicationPlan.getPrescriptions(),
                medicationPlan.getPatient());
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(
                medicationPlanDTO.getId(),
                medicationPlanDTO.getPrescriptionSet(),
                medicationPlanDTO.getPatient());
    }

}
