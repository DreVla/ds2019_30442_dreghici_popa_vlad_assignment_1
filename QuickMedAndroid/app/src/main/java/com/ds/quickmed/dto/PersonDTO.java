package com.ds.quickmed.dto;


import com.ds.quickmed.model.enums.Gender;
import com.ds.quickmed.model.enums.Role;

import java.util.Objects;

public class PersonDTO {

    private Integer id;
    private String name;
    private String birthdate;
    private Gender gender;
    private Role role;
    private String username;
    private String password;

    public PersonDTO() {
    }

    public PersonDTO(Integer id, String name, String birthdate, Gender gender, Role role, String username, String password) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.role = role;
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonDTO personDTO = (PersonDTO) o;
        return Objects.equals(id, personDTO.id) &&
                Objects.equals(name, personDTO.name) &&
                Objects.equals(birthdate, personDTO.birthdate) &&
                Objects.equals(gender, personDTO.gender) &&
                Objects.equals(role, personDTO.role) &&
                Objects.equals(password, personDTO.password) &&
                Objects.equals(username, personDTO.username);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, birthdate, gender, role);
    }
}
