package com.ds.quickmed.controller;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.ds.quickmed.model.medication.Drug;
import com.ds.quickmed.model.person.Caregiver;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.service.CaregiverService;
import com.ds.quickmed.service.DoctorService;
import com.ds.quickmed.service.DrugService;
import com.ds.quickmed.service.PatientService;
import com.ds.quickmed.utils.CaregiversRecyclerViewAdapter;
import com.ds.quickmed.utils.DrugsRecyclerViewAdapter;
import com.ds.quickmed.utils.PatientsRecyclerViewAdapter;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class DoctorController {

    public void getAllCaregivers(final Context context, final CaregiversRecyclerViewAdapter mAdapter) {
        final Call<List<Caregiver>> caregiversCall = DoctorService.getInstance().getAllCaregivers();
        caregiversCall.enqueue(new Callback<List<Caregiver>>() {
            @Override
            public void onResponse(Call<List<Caregiver>> call, Response<List<Caregiver>> response) {
                if (response.isSuccessful()) {
                    Log.d("USERLIST", String.valueOf(response.body()));
                    List<Caregiver> caregivers = response.body();
                    if(caregivers!=null){
                        mAdapter.replaceAll(caregivers);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.d("Response", "BAD");
                }
            }

            @Override
            public void onFailure(Call<List<Caregiver>> call, Throwable t) {
            }
        });
    }

    public void getAllDrugs(FragmentActivity activity, final DrugsRecyclerViewAdapter mAdapter) {
        final Call<List<Drug>> drugsCall = DoctorService.getInstance().getAllDrugs();
        drugsCall.enqueue(new Callback<List<Drug>>() {
            @Override
            public void onResponse(Call<List<Drug>> call, Response<List<Drug>> response) {
                if (response.isSuccessful()) {
                    Log.d("USERLIST", String.valueOf(response.body()));
                    List<Drug> drugs = response.body();
                    if(drugs!=null){
                        mAdapter.replaceAll(drugs);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.d("Response", "BAD");
                }
            }

            @Override
            public void onFailure(Call<List<Drug>> call, Throwable t) {
            }
        });
    }

    public void getAllPatients(FragmentActivity activity, final PatientsRecyclerViewAdapter mAdapter) {
        final Call<List<Patient>> patientsCall = DoctorService.getInstance().getAllPatients();
        patientsCall.enqueue(new Callback<List<Patient>>() {
            @Override
            public void onResponse(Call<List<Patient>> call, Response<List<Patient>> response) {
                if (response.isSuccessful()) {
                    Log.d("Success", String.valueOf(response.body()));
                    List<Patient> patients = response.body();
                    if(patients!=null){
                        mAdapter.replaceAll(patients);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.d("Response", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Patient>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getCause());
            }
        });
    }

    public void deleteCaregiver(final FragmentActivity activity, Caregiver caregiver, final CaregiversRecyclerViewAdapter mAdapter) {
        final Call<ResponseBody> sendDelete = CaregiverService.getInstance().deleteCaregiver(caregiver);
        sendDelete.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    mAdapter.notifyDataSetChanged();
                    Toast.makeText(activity, "Succes, Please Refresh", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, "Cannot delete this Caregiver", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Wrong", "onFailure: Smth wrong");
            }
        });
    }

    public void deleteDrug(final FragmentActivity activity, Drug drug, final DrugsRecyclerViewAdapter mAdapter) {
        final Call<ResponseBody> sendDelete = DrugService.getInstance().deleteDrug(drug);
        sendDelete.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    mAdapter.notifyDataSetChanged();
                    Toast.makeText(activity, "Succes, Please Refresh", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, "Cannot delete this Caregiver", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Wrong", "onFailure: Smth wrong");
            }
        });
    }

    public void deletePatient(final FragmentActivity activity, Patient patient, final PatientsRecyclerViewAdapter mAdapter) {
        final Call<ResponseBody> sendDelete = PatientService.getInstance().deletePatient(patient);
        sendDelete.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    mAdapter.notifyDataSetChanged();
                    Toast.makeText(activity, "Succes, Please Refresh", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, "Cannot delete this Caregiver", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Wrong", "onFailure: Smth wrong");
            }
        });
    }

}
