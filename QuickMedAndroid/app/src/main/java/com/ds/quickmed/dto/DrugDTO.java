package com.ds.quickmed.dto;


import com.ds.quickmed.model.medication.Prescription;

import java.util.Objects;

public class DrugDTO {

    private Integer id;
    private String name;
    private String sideeffects;
    private Prescription prescription;

    public DrugDTO() {
    }

    public DrugDTO(Integer id, String name, String sideeffects, Prescription prescription) {
        this.id = id;
        this.name = name;
        this.sideeffects = sideeffects;
        this.prescription = prescription;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideeffects() {
        return sideeffects;
    }

    public void setSideeffects(String sideeffects) {
        this.sideeffects = sideeffects;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrugDTO drugDTO = (DrugDTO) o;
        return Objects.equals(id, drugDTO.id) &&
                Objects.equals(name, drugDTO.name) &&
                Objects.equals(sideeffects, drugDTO.sideeffects) &&
                Objects.equals(prescription, drugDTO.prescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, sideeffects, prescription);
    }
}
