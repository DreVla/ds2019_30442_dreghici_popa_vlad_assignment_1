package com.ds.quickmed.view.doctor.fragments.add;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ds.quickmed.R;
import com.ds.quickmed.model.medication.MedicationPlan;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.service.DoctorService;
import com.ds.quickmed.service.PatientService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPatientActivity extends AppCompatActivity {

    private EditText username, password, name, birthdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_patient);

        username = findViewById(R.id.new_patient_username_edittext);
        password = findViewById(R.id.new_patient_password_edittext);
        name = findViewById(R.id.new_patient_name_edittext);
        birthdate = findViewById(R.id.new_patient_birthdate_edittext);

    }

    public void sendAddPatient(View view) {
        String username = this.username.getText().toString();
        String password = this.password.getText().toString();
        String name = this.name.getText().toString();
        String birthdate = this.birthdate.getText().toString();
        Patient patient = new Patient();
        patient.setUsername(username);
        patient.setPassword(password);
        patient.setName(name);
        patient.setBirthdate(birthdate);

        MedicationPlan medicationPlan = new MedicationPlan();
        final Call<ResponseBody> sendPostMedicationPlan = DoctorService.getInstance().addMedicationPlan(medicationPlan);
        sendPostMedicationPlan.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    Toast.makeText(AddPatientActivity.this, "Medication Plan also created", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddPatientActivity.this, "Medication Plan failed to create", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(AddPatientActivity.this, "Medication Plan failed to create", Toast.LENGTH_SHORT).show();
            }
        });

        patient.setMedicationPlan(medicationPlan);
        final Call<ResponseBody> sendPost = PatientService.getInstance().addNewPatient(patient);
        sendPost.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    setResult(RESULT_OK);
                    finish();
                } else {
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Wrong", "onFailure: Smth wrong");
            }
        });
    }
}
