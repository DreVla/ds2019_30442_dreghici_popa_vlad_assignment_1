package com.ds.quickmed.model.enums;
public enum Role {
    DOCTOR,
    CAREGIVER,
    PATIENT
}
