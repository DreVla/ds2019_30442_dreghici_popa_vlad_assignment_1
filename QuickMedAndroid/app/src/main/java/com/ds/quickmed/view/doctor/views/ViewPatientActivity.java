package com.ds.quickmed.view.doctor.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.ds.quickmed.R;
import com.ds.quickmed.databinding.ActivityViewPatientBinding;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.view.doctor.fragments.add.AddMedicationPlanActivity;
import com.ds.quickmed.view.doctor.viewmodel.ViewPatientViewModel;

public class ViewPatientActivity extends AppCompatActivity {

    private static final String TAG = "Found";
    private ViewPatientViewModel patientViewModel;
    private ActivityViewPatientBinding binding;
    private EditText name, birthdate;
    private Patient patient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_patient);
        patientViewModel = ViewModelProviders.of(this).get(ViewPatientViewModel.class);

        binding.setLifecycleOwner(this);
        binding.setViewPatientViewModel(patientViewModel);
        name = findViewById(R.id.patient_page_name);
        birthdate = findViewById(R.id.patient_page_birthdate);

        Bundle b = getIntent().getExtras();
        int id = b.getInt("id");
        patient = (Patient) b.getSerializable("obj");
        patientViewModel.findById(id);
        setEditTexts(false);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.details_edit_button:
                setEditTexts(true);
                break;
            case R.id.details_save_button:
                patientViewModel.sendPut(patient, name.getText().toString(), birthdate.getText().toString());
                setEditTexts(false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setEditTexts(boolean status) {
        name.setEnabled(status);
        birthdate.setEnabled(status);
    }

    public void startAddMedicationPlanView(View view) {
        Intent intent = new Intent(getApplicationContext(), AddMedicationPlanActivity.class);
        intent.putExtra("obj", patient);
        startActivity(intent);
    }
}
