package com.ds.quickmed.view.caregiver.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.ds.quickmed.R;
import com.ds.quickmed.databinding.ActivityCaregiverPatientBinding;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.view.caregiver.viewmodel.CaregiverPatientViewModel;

public class CaregiverPatientActivity extends AppCompatActivity {

    private CaregiverPatientViewModel viewModel;
    private ActivityCaregiverPatientBinding binding;
    private Patient patient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_caregiver_patient);
        viewModel = ViewModelProviders.of(this).get(CaregiverPatientViewModel.class);

        binding.setLifecycleOwner(this);
        binding.setCaregiverPatientViewModel(viewModel);

        Bundle b = getIntent().getExtras();
        int id = b.getInt("id");
        patient = (Patient) b.getSerializable("obj");
        viewModel.findById(id);
    }
}
