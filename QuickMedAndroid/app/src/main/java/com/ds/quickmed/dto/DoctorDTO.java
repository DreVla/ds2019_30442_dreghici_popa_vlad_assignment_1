package com.ds.quickmed.dto;


import java.util.Objects;
import com.ds.quickmed.model.enums.*;

public class DoctorDTO {
    private Integer id;
    private String name;
    private String birthdate;
    private Gender gender;
    private Role role;
    private String username;
    private String password;
    private String medicalRecord;

    public DoctorDTO() {
    }

    public DoctorDTO(Integer id, String name, String birthdate, Gender gender, Role role, String username, String password, String medicalRecord) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.role = role;
        this.username = username;
        this.password = password;
        this.medicalRecord = medicalRecord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorDTO doctorDTO = (DoctorDTO) o;
        return Objects.equals(id, doctorDTO.id) &&
                Objects.equals(name, doctorDTO.name) &&
                Objects.equals(birthdate, doctorDTO.birthdate) &&
                gender == doctorDTO.gender &&
                role == doctorDTO.role &&
                Objects.equals(username, doctorDTO.username) &&
                Objects.equals(password, doctorDTO.password) &&
                Objects.equals(medicalRecord, doctorDTO.medicalRecord);
    }
}
