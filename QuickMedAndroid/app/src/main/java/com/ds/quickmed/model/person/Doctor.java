package com.ds.quickmed.model.person;


import com.ds.quickmed.model.enums.Gender;
import com.ds.quickmed.model.enums.Role;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Doctor extends Person implements Serializable {

    @SerializedName("medicalRecord")
    @Expose
    private String medicalRecord;

    public Doctor() {
    }

    public Doctor(Integer id, String name, String medicalRecord) {
        super(id, name, Role.DOCTOR);
        this.medicalRecord = medicalRecord;
    }

    public Doctor(Integer id, String name, String birthdate, Gender gender, String username, String password, String medicalRecord) {
        super(id, name, birthdate, gender, Role.DOCTOR, username, password);
        this.medicalRecord = medicalRecord;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

}
