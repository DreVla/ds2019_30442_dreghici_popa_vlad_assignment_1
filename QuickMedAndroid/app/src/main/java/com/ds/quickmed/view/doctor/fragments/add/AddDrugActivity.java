package com.ds.quickmed.view.doctor.fragments.add;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.ds.quickmed.R;
import com.ds.quickmed.model.medication.Drug;
import com.ds.quickmed.service.DrugService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDrugActivity extends AppCompatActivity {

    private EditText name, sideeffects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_drug);

        name = findViewById(R.id.new_drug_name_edittext);
        sideeffects = findViewById(R.id.new_drug_sideeffects_edittext);
    }

    public void sendAddDrug(View view) {
        String name = this.name.getText().toString();
        String sideeffects = this.sideeffects.getText().toString();
        Drug drug = new Drug();
        drug.setName(name);
        drug.setSideeffects(sideeffects);
        final Call<ResponseBody> sendPost = DrugService.getInstance().addNewDrug(drug);
        sendPost.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    setResult(RESULT_OK);
                    finish();
                } else {
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Wrong", "onFailure: Smth wrong");
            }
        });
    }
}
