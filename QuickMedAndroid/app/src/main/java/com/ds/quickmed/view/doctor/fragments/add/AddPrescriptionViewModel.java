package com.ds.quickmed.view.doctor.fragments.add;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ds.quickmed.model.medication.Drug;
import com.ds.quickmed.service.DoctorService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPrescriptionViewModel extends ViewModel {

    public MutableLiveData<List<Drug>> drugsLiveData = new MutableLiveData<>();

    public void getAllDrugs() {
        final Call<List<Drug>> drugsCall = DoctorService.getInstance().getAllDrugs();
        drugsCall.enqueue(new Callback<List<Drug>>() {
            @Override
            public void onResponse(Call<List<Drug>> call, Response<List<Drug>> response) {
                if (response.isSuccessful()) {
                    Log.d("USERLIST", String.valueOf(response.body()));
                    List<Drug> drugs = response.body();
                    drugsLiveData.setValue(drugs);
                } else {
                    Log.d("Response", "BAD");

                }
            }

            @Override
            public void onFailure(Call<List<Drug>> call, Throwable t) {
            }
        });
    }
}
