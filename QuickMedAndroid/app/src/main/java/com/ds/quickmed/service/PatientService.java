package com.ds.quickmed.service;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PatientService {
    private final static String API_URL = "http://10.0.2.2:8080/";

    private static PatientApi patientApi;

    //get la instanta de retrofit
    public static PatientApi getInstance() {
        if (patientApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(provideOkHttp())
                    .build();

            patientApi = retrofit.create(PatientApi.class);
        }
        return patientApi;
    }

    private static OkHttpClient provideOkHttp() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        httpBuilder.connectTimeout(30, TimeUnit.SECONDS);

        return httpBuilder.build();
    }
}
