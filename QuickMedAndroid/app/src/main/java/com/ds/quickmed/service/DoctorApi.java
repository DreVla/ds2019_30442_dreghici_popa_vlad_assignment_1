package com.ds.quickmed.service;

import com.ds.quickmed.model.medication.Drug;
import com.ds.quickmed.model.medication.MedicationPlan;
import com.ds.quickmed.model.medication.Prescription;
import com.ds.quickmed.model.person.Caregiver;
import com.ds.quickmed.model.person.Doctor;
import com.ds.quickmed.model.person.Patient;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DoctorApi {

    @GET("/doctors/{id}")
    Call<Doctor> getDoctorById(@Path("id") Integer var);

    @GET("/doctors/username/{username}")
    Call<Doctor> getDoctorByUsername(@Path("username") String username);

    @GET("/caregivers/all")
    Call<List<Caregiver>>getAllCaregivers();

    @GET("/patients/all")
    Call<List<Patient>>getAllPatients();

    @GET("/drugs/all")
    Call<List<Drug>>getAllDrugs();


    /*
    Prescriptions
     */
    @Headers("Accept: application/json")
    @POST("/prescriptions/insert")
    Call<ResponseBody> addPrescription(@Body Prescription prescription);

    @Headers({"Accept: application/json"})
    @POST("medicationplans/insert/")
    Call<ResponseBody> addMedicationPlan(@Body MedicationPlan medicationPlan);

    @GET("medicationplans/{id}/prescriptions")
    Call<List<Prescription>> getAllPrescriptionsForMedicationPlan(@Path("id") Integer var);
}
